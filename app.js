const fs = require('fs');

console.log('Inicializado');

/*fs.readFile('data.txt', 'utf-8', (error, data) => {
    if(error){
        console.log(`Error ${error}`);
    }else{
        console.log(data);
    }
});*/

/*fs.rename('data.txt', 'dataRename.txt', (error, data) => {
    if(error) throw error;
    console.log('Renombrado');
});*/

/*fs.appendFile('data.txt', '\nPruebaAppendAsync', (error, data) => {
    if(error) console.log(`Error ${error}`);
});*/

/*fs.unlink('data2.txt', (error) => {
    if(error) throw error;
    console.log('Eliminado');
});*/

//fs.createReadStream('data.txt').pipe(fs.createWriteStream('data3.txt'));

fs.readdir('./../file-system/', (error, files) => {
    files.forEach(file => {
        console.log(file);
    });
});

console.log('Finalizado');